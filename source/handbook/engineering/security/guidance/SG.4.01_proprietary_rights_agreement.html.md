---
layout: handbook-page-toc
title: "SG.4.01 - Proprietary Rights Agreement"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SG.4.01 - Proprietary Rights Agreement

## Control Statement

All GitLabbers consent to a proprietary rights agreement.

## Context

All GitLabber's are required to sign a proprietary rights agreement prior to starting their employment with GitLab. This agreement is sent to employees in conjunction with their employment offer letter and both documents are required to be signed.

## Scope

The scope of this control is all GitLabbers.

## Ownership

* Control Owner: `Legal`
* Process owner(s):
    * Legal: `90%`
    * PeopleOps: `10%`

## Guidance

The GitLab Legal team owns the proprietary rights contractual language that all GitLabbers are required to acknowledge. PeopleOps is responsible for issuing the proprietary rights document for signature upon sending an employment offer to a candidate. For details of the agreement, refer to the [PIAA Agreements](https://about.gitlab.com/handbook/contracts/#piaa-agreements) section of the [Contracts](https://about.gitlab.com/handbook/contracts/) handbook page.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Information Security Program control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/880).

### Policy Reference
*  [PIAA Agreements](https://about.gitlab.com/handbook/contracts/#piaa-agreements)
*  [Contracts](https://about.gitlab.com/handbook/contracts/)